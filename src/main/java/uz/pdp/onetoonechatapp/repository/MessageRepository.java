package uz.pdp.onetoonechatapp.repository;
// Bahodir Hasanov 5/10/2022 11:58 AM


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.onetoonechatapp.entity.Message;
import uz.pdp.onetoonechatapp.entity.User;

import java.util.List;
import java.util.Optional;

public interface MessageRepository extends JpaRepository<Message, Integer> {
//    List<Message> findAllByReceiverId();

    List<Message> findByChatId(String chatId);

    @Query(value = "select * from messages m where m.status='UNREAD' and m.receiver_id =:userId and m.chat_id=:chatId", nativeQuery = true)
    List<Message> findAllByChatIdAndStatusUnread(Integer userId, String chatId);


}
