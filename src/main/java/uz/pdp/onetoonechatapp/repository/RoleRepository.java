package uz.pdp.onetoonechatapp.repository;
// Bahodir Hasanov 5/11/2022 11:18 AM

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.onetoonechatapp.entity.Role;

public interface RoleRepository extends JpaRepository<Role,Integer> {


}
