package uz.pdp.onetoonechatapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.onetoonechatapp.entity.User;

public interface UserRepository extends JpaRepository<User, Integer> {
    User findByPhoneNumber(String phoneNumber);
    boolean existsByPhoneNumber(String number);
}
