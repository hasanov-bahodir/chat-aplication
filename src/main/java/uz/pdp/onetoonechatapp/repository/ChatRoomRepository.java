package uz.pdp.onetoonechatapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.onetoonechatapp.entity.ChatRoom;

import java.util.Optional;

public interface ChatRoomRepository extends JpaRepository<ChatRoom, Integer> {
    Optional<ChatRoom> findByReceiverIdAndSenderId(Integer receiverId, Integer senderId);
}
