package uz.pdp.onetoonechatapp.entity;
// Bahodir Hasanov 4/29/2022 8:55 AM


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.tomcat.jni.Local;

import javax.persistence.*;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "messages")
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "text")
    private String text;
    @ManyToOne
    private User receiver;
    @ManyToOne
    private User sender;
    private String chatId;
    @Enumerated(EnumType.STRING)
    private MessageStatus status;
    private LocalDateTime time;

    public Message(String text, User receiver,User sender,String chatId,MessageStatus status,LocalDateTime time) {
        this.text = text;
        this.receiver = receiver;
        this.sender = sender;
        this.chatId = chatId;
        this.status = status;
        this.time = time;
    }

    public Message(String text) {
        this.text = text;
    }
}
