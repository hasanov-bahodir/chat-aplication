package uz.pdp.onetoonechatapp.entity;

public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
