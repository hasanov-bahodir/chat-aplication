package uz.pdp.onetoonechatapp.entity;

public enum MessageStatus {
    READ,
    UNREAD
}
