package uz.pdp.onetoonechatapp.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

import javax.persistence.*;
import java.util.UUID;

// Bahodir Hasanov 5/10/2022 9:53 AM
@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
@Entity(name = "chat_rooms")
public class ChatRoom {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    @Column(nullable = false)
    String chatId;
    @ManyToOne
    User sender;
    @ManyToOne
    User receiver;

    public ChatRoom(String chatId, User sender, User receiver) {
        this.chatId = chatId;
        this.sender = sender;
        this.receiver = receiver;
    }
}
