package uz.pdp.onetoonechatapp.common;
// Bahodir Hasanov 5/11/2022 9:22 AM

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uz.pdp.onetoonechatapp.entity.*;
import uz.pdp.onetoonechatapp.repository.ChatRoomRepository;
import uz.pdp.onetoonechatapp.repository.MessageRepository;
import uz.pdp.onetoonechatapp.repository.RoleRepository;
import uz.pdp.onetoonechatapp.repository.UserRepository;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Component
@RequiredArgsConstructor
public class DataLoader implements CommandLineRunner {
    private final UserRepository userRepository;
    private final MessageRepository messageRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    private final ChatRoomRepository chatRoomRepository;

    @Value("${spring.sql.init.mode}")
    String initMode;

    @Override
    public void run(String... args) throws Exception {
        if (initMode.equals("always")) {

            /**
             * add role
             */
            Role user = new Role(RoleName.ROLE_USER);
            roleRepository.save(user);
            Role admin = new Role(RoleName.ROLE_ADMIN);
            roleRepository.save(admin);
            Set<Role> roleSet = new HashSet<>();
            roleSet.add(user);

            /**
             * add several users
             */
            User userOne = new User("Tony", "+998901234567", passwordEncoder.encode("1"), roleSet, true);
            User userTwo = new User("Anny", "+998931234567", passwordEncoder.encode("1"), roleSet, true);
            User userThree = new User("Brad", "+998941234567", passwordEncoder.encode("1"), roleSet, true);
            userRepository.save(userOne);
            userRepository.save(userTwo);
            userRepository.save(userThree);

            /**
             * chat room
             */
            ChatRoom chatRoomOne = new ChatRoom("123e4567-e89b-12d3-a456-426614174000", userOne, userOne);
            ChatRoom chatRoomTwo = new ChatRoom("124e4567-e89b-12d3-a456-426614174000", userOne, userTwo);
            ChatRoom chatRoomTwo2 = new ChatRoom("124e4567-e89b-12d3-a456-426614174000", userTwo, userOne);
            ChatRoom chatRoomThree = new ChatRoom("125e4567-e89b-12d3-a456-426614174000", userOne, userThree);
            ChatRoom chatRoomThree2 = new ChatRoom("125e4567-e89b-12d3-a456-426614174000", userThree, userOne);
            ChatRoom chatRoomFour = new ChatRoom("126e4567-e89b-12d3-a456-426614174000", userTwo, userThree);
            ChatRoom chatRoomFour2 = new ChatRoom("126e4567-e89b-12d3-a456-426614174000", userThree, userTwo);
            chatRoomRepository.save(chatRoomOne);
            chatRoomRepository.save(chatRoomTwo);
            chatRoomRepository.save(chatRoomTwo2);
            chatRoomRepository.save(chatRoomThree);
            chatRoomRepository.save(chatRoomThree2);
            chatRoomRepository.save(chatRoomFour);
            chatRoomRepository.save(chatRoomFour2);

            /**
             * add any message
             */
            Message messageOne = new Message("Hello from One to One", userOne, userOne, "123e4567-e89b-12d3-a456-426614174000",MessageStatus.UNREAD, LocalDateTime.now());
            Message messageTwo = new Message("Hi from One to Two", userTwo, userOne,"124e4567-e89b-12d3-a456-426614174000",MessageStatus.UNREAD, LocalDateTime.now());
            Message messageThree = new Message("From One to Three ", userThree, userOne,"125e4567-e89b-12d3-a456-426614174000",MessageStatus.UNREAD, LocalDateTime.now());
            Message messageFour = new Message("Yes, from Two to Three", userThree, userTwo,"126e4567-e89b-12d3-a456-426614174000",MessageStatus.UNREAD, LocalDateTime.now());
            messageRepository.save(messageOne);
            messageRepository.save(messageTwo);
            messageRepository.save(messageThree);
            messageRepository.save(messageFour);


        }
    }
}
