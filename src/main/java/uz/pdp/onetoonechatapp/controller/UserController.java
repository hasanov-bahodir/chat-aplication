package uz.pdp.onetoonechatapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.onetoonechatapp.entity.User;
import uz.pdp.onetoonechatapp.repository.UserRepository;


import java.util.Collection;

// Bahodir Hasanov 5/10/2022 4:52 PM
@RestController
@RequestMapping("/api/users")
public class UserController {
    @Autowired
    UserRepository userRepository;
    @GetMapping
    public ResponseEntity<?> users(){
        Collection<User> userList = userRepository.findAll();
        return ResponseEntity.ok(userList);
    }

    @GetMapping("/me")
    public ResponseEntity<?> findMe(@AuthenticationPrincipal User user){
        return ResponseEntity.ok(user);
    }

}
