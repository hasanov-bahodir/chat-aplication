package uz.pdp.onetoonechatapp.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.onetoonechatapp.dto.RegisterDto;
import uz.pdp.onetoonechatapp.service.UserService;

// Bahodir Hasanov 5/10/2022 10:12 AM
@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
public class AuthController {
    public final UserService userService;

    @PostMapping("/register")
    public HttpEntity<?> register(@RequestBody RegisterDto registerDto) {
        return null;
    }

    @GetMapping("/login")
    public HttpEntity<?> login(){

        return null;
    }
}
