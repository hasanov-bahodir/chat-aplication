package uz.pdp.onetoonechatapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import uz.pdp.onetoonechatapp.dto.MessageDto;
import uz.pdp.onetoonechatapp.entity.ChatRoom;
import uz.pdp.onetoonechatapp.entity.Message;
import uz.pdp.onetoonechatapp.entity.MessageStatus;
import uz.pdp.onetoonechatapp.entity.User;
import uz.pdp.onetoonechatapp.repository.ChatRoomRepository;
import uz.pdp.onetoonechatapp.repository.MessageRepository;
import uz.pdp.onetoonechatapp.repository.UserRepository;
import uz.pdp.onetoonechatapp.service.ChatService;

import java.time.LocalDateTime;
import java.util.Optional;

// Bahodir Hasanov 4/29/2022 8:57 AM

@Controller
public class ChatController {
    @Autowired
    MessageRepository messageRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    ChatService chatService;
    @Autowired
    SimpMessagingTemplate simpMessagingTemplate;

    @MessageMapping("/hello")
//    @SendTo("/topic/chat") it is for subscribe for chat, I used it first time, but then I need create chat room between two people
    public void sendMessage(MessageDto messageDto) throws Exception {
        Thread.sleep(1000);
        Optional<User> optionalReceiver = userRepository.findById(messageDto.getReceiverId());
        Optional<User> optionalSender = userRepository.findById(messageDto.getSenderId());
        if (optionalSender.isPresent() && optionalReceiver.isPresent()) {
            Message save = messageRepository.save(
                    new Message(
                            messageDto.getText(),
                            optionalReceiver.get(),
                            optionalSender.get(),
                            chatService.getChatId(messageDto.getReceiverId(), messageDto.getSenderId(), true),
                            MessageStatus.UNREAD,
                            LocalDateTime.now()));
            messageDto.setId(save.getId());
            messageDto.setStatus(MessageStatus.UNREAD.name());
            messageDto.setTime(LocalDateTime.now());

            simpMessagingTemplate.convertAndSendToUser(
                    messageDto.getReceiverId().toString(),
                    "/queue/messages", messageDto
            );
            if (messageDto.getReceiverId() != messageDto.getSenderId()) {
                // /user/1/queue/messages
                simpMessagingTemplate.convertAndSendToUser(
                        messageDto.getSenderId().toString(),
                        "/queue/messages",
                        messageDto
                );
            }
        }
    }
}
