package uz.pdp.onetoonechatapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.onetoonechatapp.dto.MessageDto;
import uz.pdp.onetoonechatapp.entity.Message;
import uz.pdp.onetoonechatapp.entity.MessageStatus;
import uz.pdp.onetoonechatapp.entity.User;
import uz.pdp.onetoonechatapp.repository.ChatRoomRepository;
import uz.pdp.onetoonechatapp.repository.MessageRepository;
import uz.pdp.onetoonechatapp.service.ChatService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

// Bahodir Hasanov 5/10/2022 12:02 PM
@RestController
@RequestMapping("/api/messages")
public class MessageController {
    @Autowired
    MessageRepository messageRepository;
    @Autowired
    ChatService chatService;
    @Autowired
    ChatRoomRepository chatRoomRepository;
    @Autowired
    SimpMessagingTemplate simpMessagingTemplate;

    @GetMapping
    public ResponseEntity<?> getAllMessages() {
        Collection<Message> messageList = messageRepository.findAll();
        return ResponseEntity.ok(messageList);
    }

    @GetMapping("/{receiverId}")
    public ResponseEntity<?> getAllMessages(@PathVariable Integer receiverId, @AuthenticationPrincipal User currentUser) {
        String chatId = chatService.getChatId(receiverId, currentUser.getId(), false);

        if (chatId == null) {
            return ResponseEntity.ok(new ArrayList<>());
        }
        changeMessageStatusFromUnreadToRead(currentUser, chatId);
        Collection<MessageDto> messageDtoList = getMessageList(chatId);
        return ResponseEntity.ok(messageDtoList);
    }

    private Collection<MessageDto> getMessageList(String chatId) {
        List<Message> messageList = new ArrayList<>();
        messageList = messageRepository.findByChatId(chatId);
        Collection<MessageDto> messageDtoList = messageList.stream().map(message ->
                new MessageDto(
                        message.getId(),
                        message.getText(),
                        message.getReceiver().getName(),
                        message.getReceiver().getId(),
                        message.getSender().getName(),
                        message.getSender().getId(),
                        message.getStatus().name(),
                        message.getTime()
                )).collect(Collectors.toList());
        return messageDtoList;
    }

    private List<Message> changeMessageStatusFromUnreadToRead(User currentUser, String chatId) {
        Collection<Message> statusUnread = messageRepository.findAllByChatIdAndStatusUnread(currentUser.getId(), chatId);
        statusUnread.forEach(message -> message.setStatus(MessageStatus.READ));
        return messageRepository.saveAll(statusUnread);


    }
}
