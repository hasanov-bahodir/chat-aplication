package uz.pdp.onetoonechatapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import uz.pdp.onetoonechatapp.dto.RegisterDto;
import uz.pdp.onetoonechatapp.entity.User;
import uz.pdp.onetoonechatapp.repository.UserRepository;

// Bahodir Hasanov 4/30/2022 9:18 AM

@Service
public class UserService implements UserDetailsService {
    @Autowired
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String phoneNumber) throws UsernameNotFoundException {
        boolean b = userRepository.existsByPhoneNumber(phoneNumber);
        if (b)
            return userRepository.findByPhoneNumber(phoneNumber);
        throw new UsernameNotFoundException("User not found");
    }
}
