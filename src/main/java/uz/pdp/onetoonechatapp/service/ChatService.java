package uz.pdp.onetoonechatapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.onetoonechatapp.entity.ChatRoom;
import uz.pdp.onetoonechatapp.entity.User;
import uz.pdp.onetoonechatapp.repository.ChatRoomRepository;
import uz.pdp.onetoonechatapp.repository.UserRepository;

import java.util.Optional;
import java.util.UUID;

// Bahodir Hasanov 5/12/2022 11:25 AM
@Service
public class ChatService {
    @Autowired
    ChatRoomRepository chatRoomRepository;
    @Autowired
    UserRepository userRepository;

    public String getChatId(Integer receiverId, Integer senderId, boolean shouldCreateChatId) {
        Optional<ChatRoom> optionalChatRoom = chatRoomRepository.findByReceiverIdAndSenderId(receiverId, senderId);

        if (optionalChatRoom.isPresent()) {
            return optionalChatRoom.get().getChatId();
        }

        if (!shouldCreateChatId) return null;

        User receiver = userRepository.findById(receiverId).orElseThrow(() ->
                new IllegalStateException("User not found!!!"));
        User sender = userRepository.findById(senderId).orElseThrow(() ->
                new IllegalStateException("User not found!!!"));
        String chatId = UUID.randomUUID().toString();
        chatRoomRepository.save(
                new ChatRoom(
                        chatId,
                        receiver,
                        sender
                )
        );

        if (!sender.equals(receiver)) chatRoomRepository.save(
                new ChatRoom(
                        chatId,
                        sender,
                        receiver
                )
        );

        return chatId;

    }
}
