package uz.pdp.onetoonechatapp.dto;
// Bahodir Hasanov 5/10/2022 3:43 PM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class MessageDto {
    private Integer id;
    private String text;
    private String receiverName;
    private Integer receiverId;
    private String senderName;
    private Integer senderId;
    private String status;
    private LocalDateTime time;
}
