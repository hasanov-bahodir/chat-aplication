package uz.pdp.onetoonechatapp.dto;
// Bahodir Hasanov 5/10/2022 10:45 AM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
@AllArgsConstructor
@NoArgsConstructor
@Data
public class RegisterDto {
    String firstName;
    String lastName;
    String phoneNumber;
}
