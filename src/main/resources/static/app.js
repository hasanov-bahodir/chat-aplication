var stompClient = null;
var currentUser = null;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    } else {
        $("#conversation").hide();
    }
    $("#messages").html("");
}

// function connect() {
//     var socket = new SockJS('/ws-endpoint');
//     stompClient = Stomp.over(socket);
//     stompClient.connect({}, function (frame) {
//         setConnected(true);
//         // getMessages();
//         getUsers();
//         getCurrentUser();
//         console.log('Connected: ' + frame);
//         stompClient.subscribe('/topic/chat', function (message) {
//             showGreeting(JSON.parse(message.body));
//         });
//     });
// }

async function connect() {
    await getCurrentUser();
    var socket = new SockJS('/ws-endpoint');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        getUsers();
        console.log('Connected: ' + frame);
        stompClient.subscribe(
            '/user/' + currentUser.id + '/queue/messages',
            function (messageDto) {
                const data = JSON.parse(messageDto.body)
                if (data.senderId === currentUser.id || data.senderId==$("#users").val())
                    appendNewMessage(data)
                else
                    alert("New message :: " + data.receiverName+"->"+data.text)
            });
    });
}

async function getCurrentUser() {
    await fetch("/api/users/me")
        .then(function (response) {
            if (response.ok) {
                response.json().then(data => {
                    currentUser = data;
                    $('#welcome').append(currentUser.name)
                })
            }
        })
}


function getUsers() {
    $("#users").empty();
    fetch("/api/users")
        .then(function (response) {
            if (response.ok) {
                response.json().then(res => {
                    const firstUserId = res[0].id;
                    getMessagesById(firstUserId);
                    res.map(value =>
                        appendUser(value))
                })
            }
        })
}

function getMessages() {
    console.log("Messages are coming")
    fetch("/api/messages")
        .then(function (response) {
            if (response.ok) {
                response.json().then(res => {
                    console.log(res);
                    res.map(value => {
                        appendNewMessage(value)
                    })
                })
            }
        })
}

function userChangeHandle(selectedObj) {
    const selectedUserId = selectedObj.value;
    console.log(selectedUserId);
    getMessagesById(selectedUserId);
}

function getMessagesById(firstUserId) {
    // console.log($("#users").val());
    fetch("/api/messages/" + firstUserId)
        .then(function (response) {
            if (response.ok) {
                response.json().then(res => {
                    $("#messages").empty()
                    res.map(value => {
                        appendNewMessage(value)
                    })
                })
            }
        })
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function sendMessage() {
    stompClient.send("/app/hello", {}, JSON.stringify({
        'text': $("#text").val(),
        'receiverName': $("#users").find(":selected").text(),
        'receiverId': $("#users").val(),
        'senderName': currentUser.name,
        'senderId': currentUser.id
    }));
}

function appendNewMessage(message) {
    console.log(message);
    let tdAlign = message.senderId === currentUser.id ? 'right' : 'left';
    let status = message.status === 'UNREAD' ? '✔ (unread)' : '✔✔ (read)';
    $("#messages").append("<tr>" + "<td align='" + tdAlign + "'>" +
        "<h4>" + message.text + "</h4>" +
        "<h6>" + message.senderName + "</h6>" +
        "<h6>" + status + "</h6>" +
        "<h6>" + message.time + "</h6>" +
        "</td>" + "</tr>");
}

function appendUser(user) {
    $("#users").append("<option value=" + user.id + ">" + user.name + "</option>");
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $("#connect").click(function () {
        connect();
    });
    $("#disconnect").click(function () {
        disconnect();
    });
    $("#send").click(function () {
        sendMessage();
    });
});